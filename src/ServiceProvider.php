<?php
namespace KDA\Laravel\Morph;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasHelper;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasHelper;
    protected $packageName ='laravel-morph-helpers';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){

        Collection::macro('whereHasMorph', function ($key, string |Model $value) {
            return $this->where("{$key}_type", get_morph_class($value));
        });

        Collection::macro('whereMorphedTo', function ($key,  Model $value) {
            return $this->where("{$key}_type", get_morph_class($value))
                ->where("{$key}_id", $value->getKey());
        });

    }
}
