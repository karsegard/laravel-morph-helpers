<?php

use Illuminate\Database\ClassMorphViolationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

if (!function_exists('get_morph_class')) {

    function get_morph_class(string | Model $model)
    {
        if (is_string($model)) {
            $reflection = new ReflectionClass($model);
            if (!$reflection->isSubclassOf(Model::class)) {
                throw new InvalidArgumentException("{$model} is not a Model}");
            }
            $model = new $model;
        }
        try {
            return $model->getMorphClass();
        } catch (ClassMorphViolationException $e) {
            if (Relation::requiresMorphMap()) {
                throw new ClassMorphViolationException($model);
            }
            return $model;
        }
    }
}
